extends Node2D

const Enums = preload("res://scripts/enums.gd")

var speed = 30
var target:Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area.connect("area_entered",self,"collide")
	pass # Replace with function body.



func _process(delta):
	if(target!=null):
		var dir = (target).normalized()
		position += dir * speed * delta
	pass

func collide(a):
	var enemy = a.get_parent()
	if(enemy.type==Enums.EnemyType.MINE || enemy.type==Enums.EnemyType.SHIP):
		enemy.hurt()
		queue_free()
	pass
