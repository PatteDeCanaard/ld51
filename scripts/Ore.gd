extends Node2D


onready var main:Node2D = get_node("/root").find_node("Main",true,false)
onready var selector:Node2D = get_node("/root").find_node("ItemSelector",true,false)
onready var scene:Node2D = get_node("/root").find_node("CurrentSceneContainer",true,false)
onready var planet = get_node("/root").find_node("Planet",true,false)
onready var players:Node2D = get_node("/root").find_node("Players",true,false)
onready var bullet_scene = preload("res://scenes/EnemyBullet.tscn")

const Enums = preload("res://scripts/enums.gd")

var isGrabed = false
var isDestroyed = false
var type = Enums.EnemyType.JEWEL
var isTargeted = false
var layer = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if isDestroyed:
		return
	if(type==Enums.EnemyType.JEWEL || type==Enums.EnemyType.HUMAN):
		computeJewel(delta)
	if(type==Enums.EnemyType.MINE):
		computeMine(delta)
	if(type==Enums.EnemyType.SHIP):
		computeShip(delta)
		
func computeJewel(delta):
	if isGrabed || isDestroyed:
		return
	var mpos = (main.get_viewport().get_mouse_position()/4)
	mpos = main.to_local(mpos)
	if mpos.distance_to(main.to_local(global_position))<10:
		selector.select(self)
	else:
		selector.deselect(self)
	if position.length()<30:
		die(false)
	
func computeMine(delta):
	if position.length()<30:
		die(true,5)
		
		
var fire_rate = 2.0
var fire_time = 0
func computeShip(delta):
	if position.length()<30:
		die(true,5)
	if fire_time>fire_rate:
		fire()
		fire_time = 0
	else:
		fire_time += delta
		
func recolt():
	if isGrabed:
		return
	isGrabed = true
	main.current_pivot_player.fish(self)
	pass

func jewelDie():
	main.set_jewelCount(main.jewelCount + 1)

func die(effect=true, delay=0):
	if effect:
		if type==Enums.EnemyType.JEWEL:
			jewelDie()
		if type==Enums.EnemyType.HUMAN:
			main.add_player()
		if type == Enums.EnemyType.MINE:
			planet.hurt(25)
		if type == Enums.EnemyType.SHIP:
			planet.hurt(10)

	isDestroyed = true
	for p in players.get_children():
		p.clear_enemy_target(self)
		
	selector.deselect(self)
	$Sprite.visible = false
	$ProgressBar.visible = false
	
	if type==Enums.EnemyType.MINE || type==Enums.EnemyType.SHIP:
		$ExplosionSound.play()
		$ExplosionParticles.restart()
		PDCTools.screenshake(5,0.5)
	
	if delay>0:
		yield(get_tree().create_timer(delay), "timeout")
	queue_free()

func set_type(t):
	type = t
	for c in $Sprite.get_children():
		c.visible = false
	if(t==Enums.EnemyType.JEWEL):
		$Sprite/Ore3.visible = true
	if(t==Enums.EnemyType.MINE):
		$Sprite/Mine.visible = true
		init_pdv(5)
		$ProgressBar.visible = true
	if(t==Enums.EnemyType.SHIP):
		$Sprite/Ship.visible = true
		init_pdv(7)
		$ProgressBar.visible = true
	if(t==Enums.EnemyType.HUMAN):
		$Sprite/Player.visible = true
	if(t!=Enums.EnemyType.MINE && t!=Enums.EnemyType.SHIP):
		$Area2D.queue_free()
	pass
	
var pdv = 5
func init_pdv(maxpdv):
	$ProgressBar.max_value = maxpdv
	pdv = maxpdv
	$ProgressBar.value = pdv

func hurt():
	if isDestroyed:
		return
	if type==Enums.EnemyType.MINE:
		$AnimationPlayer2.play("mine_hurt")
		$HurtSound.play()
	if type==Enums.EnemyType.SHIP:
		$AnimationPlayer2.play("ship_hurt")
		$HurtSound.play()
	pdv -= 1
	$ProgressBar.value = pdv
	if pdv == 0:
		 die(false, 5)
		
func fire():
	if layer!=null && layer!="layer5" && layer!="layer4" && layer!="layer0" && layer!="layer3":
		var i = bullet_scene.instance()
		planet.add_child(i)
		i.global_position = global_position
		i.target = i.to_local(planet.global_position)
