extends Node2D

const Enums = preload("res://scripts/enums.gd")

onready var planet:Node2D = get_node("/root").find_node("Planet",true,false)

var speed = 30
var target:Vector2

var isDestroyed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area.connect("area_entered",self,"collide")
	pass # Replace with function body.



func _process(delta):
	if(target!=null):
		var dir = (target).normalized()
		position += dir * speed * delta
	pass

func collide(a):
	if isDestroyed:
		return
	isDestroyed = true
	planet.hurt(1)
	$ExplosionParticles.restart()
	$Sprite.visible = false
	yield(get_tree().create_timer(1.0), "timeout")
	queue_free()
	pass
