extends Node2D

const Enums = preload("res://scripts/enums.gd")
onready var bullet_scene = preload("res://scenes/Bullet.tscn")

export var rotation_rad_speed = 0.4
export var time_sand_by_iteration = 2
export var time_recolt = 1

onready var main:Node2D = get_node("/root").find_node("Main",true,false)
onready var sablierTop:Node2D = get_node("/root").find_node("SablierTop",true,false)
onready var sablierBottom:Node2D = get_node("/root").find_node("SablierBottom",true,false)
onready var planet:Node2D = get_node("/root").find_node("Planet",true,false)
onready var items:Node2D = get_node("/root").find_node("Items",true,false)

var speedLevel = 0
var defenseLevel = 0
var fishingLevel = 0

var fishing_speed = 10
var fire_rate = 1.5

var isRunning = false
var target_rad = 0
var hasSand = false
var isFishing = false

var face = 0 

var mode = Enums.PlayerMode.ACCELERATE

var time_fire = 0

var player_name = "?"
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	face = randi()%7
	var ri = randi()%main.names.size()
	player_name = main.names[ri]
	main.names.remove(ri)
	pass # Replace with function body.

var elapsed = 0.0
var init_rotation = 0.0
var last_rotation = 0.0
var recolting = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#	input
	if Input.is_action_just_pressed("action"):
		pass
		
	if !isFishing && mode == Enums.PlayerMode.DEFEND:
		if current_enemy_target==null:
			isRunning = false
			rotation += delta * 0.4
			$Player/PlayerRun.flip_h = true
			current_enemy_target = find_enemy()
			if current_enemy_target!=null:
				current_enemy_target.isTargeted = true
				goTo(current_enemy_target.global_position)
		else:
			if !isRunning:
				if time_fire>fire_rate:
					fire()
					time_fire = 0
				else:
					time_fire += delta
		
	#	running
	if isRunning:
		rotation = lerp_angle(init_rotation, target_rad, elapsed)
		if rotation-last_rotation<0:
			$Player/PlayerRun.flip_h = false
		else:
			$Player/PlayerRun.flip_h = true
		last_rotation = rotation
		elapsed += rotation_rad_speed*delta
		if elapsed>1:
			isRunning = false
#		print("[target: "+str(target_rad)+"] "+"[rotation: "+str(rotation)+"]")

	if isFishing:
		if !isRunning:
			$Player/Line2D.points = [$Player.to_local(fishing_target.global_position), position]
			var dir = ($Player.global_position-fishing_target.global_position).normalized()
			fishing_target.global_position += dir * fishing_speed * delta
			if fishing_target.global_position.distance_to($Player.global_position)<3:
				fishing_target.die()
				$AudioStreamPlayer.play()
				$Player/Line2D.points = []
				fishing_target = null
				isFishing = false
				if current_enemy_target != null:
					current_enemy_target.isTargeted = false
					current_enemy_target = null

			
	#process
	if(!isFishing):
		sandProcess(delta)
		timeUpProcess(delta)

	
	pass
	
func goTo(gpos:Vector2, fromUI = false):
	if fromUI:
		gpos = gpos/4
	var lpos = get_parent().to_local(gpos)
	target_rad = Vector2.UP.angle_to(lpos)
#	print("[target: "+str(target_rad)+"]")
	isRunning = true
	elapsed = 0.0
	init_rotation = rotation
	
func sandProcess(delta:float):
	if isRunning || recolting:
		return

	if $Player/PlayerRun.global_position.distance_to(sablierBottom.global_position)<50:
		if mode == Enums.PlayerMode.SLOW_DOWN && !hasSand:
			isRunning = false
			recolting = true
			planet.add_time_bottom(-time_sand_by_iteration)
			yield(get_tree().create_timer(time_recolt), "timeout")
			recolting = false
			hasSand = true
		elif mode == Enums.PlayerMode.ACCELERATE && hasSand:
			isRunning = false
			recolting = true
			planet.add_time_bottom(time_sand_by_iteration)
			yield(get_tree().create_timer(time_recolt), "timeout")
			recolting = false
			hasSand = false
#			print("recolt sand")
	if $Player/PlayerRun.global_position.distance_to(sablierTop.global_position)<50:
		if mode == Enums.PlayerMode.SLOW_DOWN && hasSand:
			isRunning = false
			recolting = true
			planet.add_time_top(-time_sand_by_iteration)
			yield(get_tree().create_timer(time_recolt), "timeout")
			recolting = false
			hasSand = false

		if mode == Enums.PlayerMode.ACCELERATE && !hasSand:
			isRunning = false
			recolting = true
			planet.add_time_top(time_sand_by_iteration)
			yield(get_tree().create_timer(time_recolt), "timeout")
			recolting = false
			hasSand = true
#			print("add sand")
	

func timeUpProcess(delta:float):
	if isRunning || recolting:
		return
	if mode == Enums.PlayerMode.SLOW_DOWN:
		if !hasSand:
			goTo(sablierBottom.global_position)
		if hasSand:
			goTo(sablierTop.global_position)
	if mode == Enums.PlayerMode.ACCELERATE:
		if hasSand:
			goTo(sablierBottom.global_position)
		if !hasSand:
			goTo(sablierTop.global_position)

var fishing_target
func fish(item:Node2D):
	if isFishing:
		return
	isFishing = true
	fishing_target = item
	if current_enemy_target!=null:
		current_enemy_target.isTargeted = false
		current_enemy_target = null
	goTo(item.global_position)

func get_cost(v):
	return pow(2,v)
	
func increaseSpeedLevel():
	speedLevel += 1
	rotation_rad_speed += 0.1
	
func increaseDefenseLevel():
	defenseLevel += 1
	fire_rate = max(0.1, fire_rate-0.2)
	
func increaseFishingLevel():
	fishingLevel += 1
	fishing_speed += 10
	
var current_enemy_target = null
func find_enemy():
	for l in items.get_children():
		var result = null
		for c in l.get_children():
			if l.name!= "layer5" && l.name!= "layer4":
				if (c.type == Enums.EnemyType.MINE || c.type == Enums.EnemyType.SHIP) && !c.isDestroyed:
					if !c.isTargeted:
						return c
					else:
						result = c
		if result != null:
			return
	return null
	
func clear_enemy_target(e):
	if current_enemy_target==e:
		current_enemy_target = null

func fire():
	if current_enemy_target==null:
		return
	var i = bullet_scene.instance()
	get_parent().get_parent().add_child(i)
	i.global_position = $Player.global_position
	i.target = current_enemy_target.position
