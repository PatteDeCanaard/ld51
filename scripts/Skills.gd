extends Control


onready var main:Node2D = get_node("/root").find_node("Main",true,false)


# Called when the node enters the scene tree for the first time.
func _ready():
	$SpeedLabel/Boost/Button.connect("pressed",self,"buySpeed")
	$DefenseLabel/Boost/Button.connect("pressed",self,"buyDefense")
	$FishingLabel/Boost/Button.connect("pressed",self,"buyFishing")
	pass # Replace with function body.


func update_ui():
	var jewelCount = main.jewelCount
	#speed
	var speedLevel = main.current_pivot_player.speedLevel
	var speedCost = main.current_pivot_player.get_cost(speedLevel)
	$SpeedLabel/CostLabel.text = str(speedCost)
	$SpeedLabel/LevelLabel.text = "("+str(speedLevel)+")"
	if(jewelCount>=speedCost):
		$SpeedLabel/Boost.visible = true
	else:
		$SpeedLabel/Boost.visible = false
	#defense
	var defenseLevel = main.current_pivot_player.defenseLevel
	var defenseCost = main.current_pivot_player.get_cost(defenseLevel)
	$DefenseLabel/CostLabel.text = str(defenseCost)
	$DefenseLabel/LevelLabel.text = "("+str(defenseLevel)+")"
	if(jewelCount>=defenseCost):
		$DefenseLabel/Boost.visible = true
	else:
		$DefenseLabel/Boost.visible = false
	#fishing
	var fishingLevel = main.current_pivot_player.fishingLevel
	var fishingCost = main.current_pivot_player.get_cost(fishingLevel)
	$FishingLabel/CostLabel.text = str(fishingCost)
	$FishingLabel/LevelLabel.text = "("+str(fishingLevel)+")"
	if(jewelCount>=fishingCost):
		$FishingLabel/Boost.visible = true
	else:
		$FishingLabel/Boost.visible = false

func buySpeed():
	var speedLevel = main.current_pivot_player.speedLevel
	var speedCost = main.current_pivot_player.get_cost(speedLevel)
	main.current_pivot_player.increaseSpeedLevel()
	main.set_jewelCount(main.jewelCount-speedCost)
	main.update_ui()

func buyDefense():
	var defenseLevel = main.current_pivot_player.defenseLevel
	var defenseCost = main.current_pivot_player.get_cost(defenseLevel)
	main.current_pivot_player.increaseDefenseLevel()
	main.set_jewelCount(main.jewelCount-defenseCost)
	main.update_ui()

func buyFishing():
	var fishingLevel = main.current_pivot_player.fishingLevel
	var fishingCost = main.current_pivot_player.get_cost(fishingLevel)
	main.current_pivot_player.increaseFishingLevel()
	main.set_jewelCount(main.jewelCount-fishingCost)
	main.update_ui()
