extends Node2D

onready var ore = preload( "res://scenes/Ore.tscn")
const Enums = preload("res://scripts/enums.gd")

var radius = 10

var waves = [
	["j","j"],
	["j"],
	["j","j"],
	[],
	["j","h"],
	["j"],
	["j"],
	["j","m"],
	["j"],
	["m","j"],
	["j","m","j"],
	["m","m"],
	["j","j"],
	[],
	["s"],
	["h"],
	["j","j", "m", "s"],
	["s","j", "m"],
	["j","j", "j"],
	["m","j","m","j","s"],
	["m","j","s","j"],
	["s","j","s"],
	["m","j","s","j","s"],
	["h"],
	["m","m","m"],
	["j","j"],
	["m","j","s","j","s","m"],
	["m","m","m","m","m"],
	["j","j"],
	["s","s","s","m","m"],
	["j","j", "s","s","s","m","m"],
	["m","m","m","m","m"],
	["j","j", "s","s","s","m","m","m","m"],
	["j","j", "s","j","s","j","m","m","m"],
	["j","h", "s","s","s","s","s","s","s"],
	["j","j", "s","s","s","s","s","s","s"],
]

var current_wave_i = 0
var ll = 150

#50, 72, 90 
# Called when the node enters the scene tree for the first time.
func _ready():
	next_wave()
	pass # Replace with function body.

func _process(delta):
#	if Input.is_action_just_pressed("ui_accept"):
#		pulse()
	pass

func next_wave():
	var wave = waves[current_wave_i]
	if current_wave_i+1<waves.size():
		current_wave_i += 1
	for i in wave:
		if i=="h":
			spawn(ore, ll, Enums.EnemyType.HUMAN)
		if i=="m":
			spawn(ore, ll, Enums.EnemyType.MINE)
		if i=="s":
			spawn(ore, ll, Enums.EnemyType.SHIP)
		else:
			spawn(ore, ll, Enums.EnemyType.JEWEL)

func spawn(s:PackedScene, radius:float, t = Enums.EnemyType.JEWEL):
	var i = s.instance()
	
	#pos
	var teta = rand_range(0,2*PI)
	var r = radius
	var x = int(r*sin(teta))
	var y = int(r*cos(teta))
	i.position = Vector2(x,y)
	i.set_type(t)
	$layer5.add_child(i)
	

func pulse():
	var tween = $Tween
	var arr = [20,52,70,90,112,150]
	for i in range(1,6):
		for c in get_node("layer"+str(i)).get_children():
			if !c.isGrabed && !c.isDestroyed:
				var distance = arr[i]-arr[i-1]
				var dir = (-c.position).normalized()
				var newpos = c.position + dir * distance
				newpos = Vector2(int(newpos.x),int(newpos.y))
				tween.interpolate_property(c, "position", c.position, newpos, 1,
				Tween.TRANS_ELASTIC, Tween.EASE_OUT)
				c.layer = "layer"+str(i)
				
	for i in range(1,6):
		var layer = get_node("layer"+str(i))
		var nextlayer = get_node("layer"+str(i-1))
		for c in layer.get_children():
			if !c.isGrabed && !c.isDestroyed:
				c.layer = "layer"+str(i-1)
				layer.remove_child(c)
				nextlayer.add_child(c)
	tween.start()
	next_wave()
