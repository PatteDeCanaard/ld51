extends Node2D

onready var main:Node2D = get_node("/root").find_node("Main",true,false)
var item:Node2D=null;


# Called when the node enters the scene tree for the first time.
func _ready():
	$Button.connect("pressed",self,"on_fish")
	pass # Replace with function body.

func select(i):
	if main.current_pivot_player==null:
		return
	if main.current_pivot_player.isFishing:
		return
	item = i

func deselect(i):
	if item==i:
		item = null
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if main.current_pivot_player==null:
		return
	if item!=null && !item.isDestroyed:
		global_position = item.global_position*4
	if main.current_pivot_player.isFishing:
		return
	if item!=null:
		visible = true
	else:
		visible = false
	pass

func on_fish():
	if(item!=null):
		item.recolt()
