extends Control


onready var main:Node2D = get_node("/root").find_node("Main",true,false)
const Enums = preload("res://scripts/enums.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Accelerate/Button.connect("pressed",self,"set_accelerate")
	$Slow/Button.connect("pressed",self,"set_slow")
	$Defend/Button.connect("pressed",self,"set_mode_defend")
	pass # Replace with function body.

func reset():
	$Accelerate.self_modulate.a = 0.1
	$Slow.self_modulate.a = 0.1
	$Defend.self_modulate.a = 0.1
	
func set_accelerate():
	reset()
	$Accelerate.self_modulate.a = 1
	main.set_mode_accelerate()
	
func set_slow():
	reset()
	$Slow.self_modulate.a = 1
	main.set_mode_slow()


func set_mode_defend():
	reset()
	$Defend.self_modulate.a = 1
	main.set_mode_defend()

func update_ui():
	var mode = main.current_pivot_player.mode 
	reset()
	if mode == Enums.PlayerMode.ACCELERATE:
		$Accelerate.self_modulate.a = 1
	if mode == Enums.PlayerMode.SLOW_DOWN:
		$Slow.self_modulate.a = 1
	if mode == Enums.PlayerMode.DEFEND:
		$Defend.self_modulate.a = 1
