extends Node2D

onready var main:Node2D = get_node("/root").find_node("Main",true,false)
onready var items:Node2D = get_node("/root").find_node("Items",true,false)

signal timer_end

var pdv = 100

var isPlanet = true
var pulse_counter = 0

func hurt(d):
	pdv -= d
	if pdv<=75:
		$Fissures/Fissure1.visible = true
	if pdv<=50:
		$Fissures/Fissure2.visible = true
	if pdv<=25:
		$Fissures/Fissure3.visible = true
	if pdv<=0:
		destroy()

# Called when the node enters the scene tree for the first time.
func _ready():
	pulse()
	pass # Replace with function body.

var isRotating = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if isDestroy:
		return
	if !isRotating:
		rotate_sablier()
	process_time(delta)
	
#	if Input.is_action_just_pressed("ui_accept"):
#		destroy()
	pass

func rotate_sablier():
	isRotating = true
	var start = 0
	var end = 180
	var tween = get_node("TweenRotation")
	tween.interpolate_property($Rotation, "rotation_degrees",
		start, end, 1,
		Tween.TRANS_ELASTIC, Tween.EASE_IN_OUT)
		
	tween.start()
	yield(tween,"tween_all_completed")
	$Rotation.rotation_degrees = 0
	$Rotation/AnimationPlayer.play("fade_out")
	yield($Rotation/AnimationPlayer,"animation_finished")
	
#	$Rotation/Sand/AnimationPlayer.play("timer")
#	yield($Rotation/Sand/AnimationPlayer,"animation_finished")
	activate()
	yield(self,"timer_end")
	$Rotation/AnimationPlayer.play("fade_in")
	yield($Rotation/AnimationPlayer,"animation_finished")
	reset_time()
	isRotating = false
#	yield(get_tree().create_timer(.5), "timeout")
#	$Rotation/Sand.rotation_degrees = 180

var time = 0
var timeBottom = 0
var timeBank = 0
var isActive = false

var hourglass_delay = 10

func process_time(delta:float):
	if !isActive:
		return
	if time>hourglass_delay:
		isActive = false
		$Rotation/Sand/CPUParticles2D.emitting = false
		pulse()
		emit_signal("timer_end")
	if isActive:
		time += delta
		timeBottom += delta
		var ftop = time/hourglass_delay;
		var fbottom = timeBottom/hourglass_delay;
		$Rotation/Sand/SandTop.material.set_shader_param("_Threshold", ftop)
		$Rotation/Sand/SandBottom.material.set_shader_param("_Threshold", 1.0-fbottom)
		
func reset_time():
		time = timeBank
		timeBank = 0
		timeBottom = 0
		$Rotation/Sand/SandTop.material.set_shader_param("_Threshold", 0)
		$Rotation/Sand/SandBottom.material.set_shader_param("_Threshold", 1)
func activate():
	isActive=true
	$Rotation/Sand/CPUParticles2D.emitting = true
	
func add_time_top(v):
	if v<0:
		$Rotation/Sand/AnimationPlayer.play("add_sand")
	else:
		$Rotation/Sand/AnimationPlayer.play("remove_sand")
	yield(get_tree().create_timer(.5), "timeout")
	if isActive:
		time += v
	else:
		timeBank += v
	pass

func add_time_bottom(v):
	if isActive:
		timeBottom += v
		timeBottom = max(timeBottom,0)
	pass

func pulse():
	pulse_counter += 1
	main.shockwave()
	$ShockWaveSound.play()
#	yield(get_tree().create_timer(.3), "timeout")
	items.pulse()

var isDestroy = false
func destroy():
	if(isDestroy):
		return
	isDestroy = true
	main.endGame()
