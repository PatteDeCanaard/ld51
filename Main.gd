extends Node2D

const Enums = preload("res://scripts/enums.gd")
onready var player_scene = preload( "res://scenes/PivotPlayer.tscn")
onready var selector_scene = preload( "res://scenes/Selector.tscn")

var current_pivot_player

var jewelCount = 0

var names = [
	"Richard",
	"Camembert",
	"Giorno",
	"Rose",
	"Guigou",
	"Jackson"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	$UI/SoundButton.connect("pressed",self,"toggle_audio")
	randomize()
	set_player(get_node("/root").find_node("PivotPlayer",true,false))
	start_tuto()
	$UI/PlayerInfos/Switch/Next.connect("pressed",self,"next_player")
	$UI/PlayerInfos/Switch/Previous.connect("pressed",self,"previous_player")
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_just_pressed("full_screen"):
		OS.window_fullscreen = !OS.window_fullscreen
	if Input.is_action_just_pressed("ui_cancel"):
		OS.window_fullscreen = false
	if waitRestart && Input.is_action_just_pressed("action"):
		get_tree().reload_current_scene()

func set_player(p):
	if current_pivot_player==p:
		return
	current_pivot_player = p
	if p==null:
		$UI.queue_free()
		return
	var p_name = get_node("/root").find_node("PlayerName",true,false)
	p_name.text = p.player_name
	update_ui()
	$UI/AnimationPlayer.play("fade_in")
	
func set_mode_accelerate():
	current_pivot_player.mode = Enums.PlayerMode.ACCELERATE
	update_ui()

func set_mode_slow():
	current_pivot_player.mode = Enums.PlayerMode.SLOW_DOWN
	update_ui()
	
func set_mode_defend():
	current_pivot_player.mode = Enums.PlayerMode.DEFEND
	update_ui()

func shockwave():
	$AnimationPlayer.play("shockwave")

func set_jewelCount(v):
	jewelCount = v
	update_ui()
	
func update_ui():
	for c in $UI/PlayerInfos/Faces.get_children():
		c.visible = false
	get_node("UI/PlayerInfos/Faces/face"+str(current_pivot_player.face)).visible = true
	$UI/Jewels/Label.text = str(jewelCount)
	$UI/PlayerInfos/Skills.update_ui()
	$UI/PlayerInfos/Jobs.update_ui()
	for c in $UI/Selectors.get_children():
		c.update_ui()
	
func add_player():
	var i_player = player_scene.instance()
	$PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/Planet/Players.add_child(i_player)
	var r = randi()%2
	if r==0:
		i_player.rotation_degrees = 90
	else:
		i_player.rotation_degrees = -90
	var i_selector = selector_scene.instance()
	i_selector.pivotPlayer = i_player
	$UI/Selectors.add_child(i_selector)

var waitRestart = false
func endGame():
	var score = $PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/Planet.pulse_counter
	$UIEnd/Control/score.text = "You survived "+str(score)+" beats"
	$UI.queue_free()
	$PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/Planet.queue_free()
	PDCTools.screenshake(10,0.5)
	$PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/PlanetExplosionParticles.restart()
	$AnimationPlayer.play("endgame")

	yield(get_tree().create_timer(1.0), "timeout")
	waitRestart= true

var mute_audio = false
func toggle_audio():
	mute_audio = !mute_audio
	AudioServer.set_bus_mute(0,mute_audio)
	
func clear_tuto():
	for c in $Tuto.get_children():
		c.visible = false
	
func start_tuto():
	#1
	clear_tuto()
	$Tuto/Label1.visible = true
	$TutoAnim.play("fade_in")
	yield(get_tree().create_timer(13), "timeout")
	$TutoAnim.play("fade_out")
	yield($TutoAnim, "animation_finished")
	yield(get_tree().create_timer(2), "timeout")
	#2
	clear_tuto()
	$Tuto/Label2.visible = true
	$TutoAnim.play("fade_in")
	yield(get_tree().create_timer(10), "timeout")
	$TutoAnim.play("fade_out")
	yield($TutoAnim, "animation_finished")
	#3
	clear_tuto()
	$Tuto/Label3.visible = true
	$TutoAnim.play("fade_in")
	yield(get_tree().create_timer(7), "timeout")
	$TutoAnim.play("fade_out")
	yield($TutoAnim, "animation_finished")
	#4
	clear_tuto()
	$Tuto/Label4.visible = true
	$TutoAnim.play("fade_in")
	yield(get_tree().create_timer(10), "timeout")
	$TutoAnim.play("fade_out")
	yield($TutoAnim, "animation_finished")
	yield(get_tree().create_timer(5), "timeout")
	#5
	clear_tuto()
	$Tuto/Label5.visible = true
	$TutoAnim.play("fade_in")
	yield(get_tree().create_timer(10), "timeout")
	$TutoAnim.play("fade_out")
	yield($TutoAnim, "animation_finished")

func next_player():
	var players:Array = $PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/Planet/Players.get_children()
	if players.size()<=1:
		return
	var i = players.find(current_pivot_player,0)
	if i+1<players.size():
		set_player(players[i+1])
	else:
		set_player(players[0])

func previous_player():
	var players:Array = $PDCMain/ViewportContainer/Viewport/CurrentSceneContainer/scene/Planet/Players.get_children()
	if players.size()<=1:
		return
	var i = players.find(current_pivot_player,0)
	if i-1>=0:
		set_player(players[i-1])
	else:
		set_player(players[players.size()-1])
