extends Node2D

onready var main:Node2D = get_node("/root").find_node("Main",true,false)
onready var planet:Node2D = get_node("/root").find_node("Planet",true,false)
const Enums = preload("res://scripts/enums.gd")
onready var pivotPlayer:Node2D
var offset = Vector2(0,5)
# Called when the node enters the scene tree for the first time.
func _ready():
	$TextureButton.connect("pressed",self,"set_player")
	if pivotPlayer==null:
		pivotPlayer = get_node("/root").find_node("PivotPlayer",true,false)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if planet.isDestroy:
		return
	if main.current_pivot_player==pivotPlayer:
		$Sprites/CircleIndicator.visible = false
		$Sprites/Arrow.visible = true
	else:
		$Sprites/CircleIndicator.visible = true
		$Sprites/Arrow.visible = false
	global_rotation =  pivotPlayer.get_node("Player").global_rotation
	global_position = pivotPlayer.get_node("Player").to_global(offset)*4
	pass

func set_player():
	main.set_player(pivotPlayer)
	
func update_ui():
	for c in $PlayerMode.get_children():
		c.visible = false
	if pivotPlayer.mode == Enums.PlayerMode.ACCELERATE:
		$PlayerMode/Sablierplus.visible = true
	if pivotPlayer.mode == Enums.PlayerMode.SLOW_DOWN:
		$PlayerMode/Sabliermoin.visible = true
	if pivotPlayer.mode == Enums.PlayerMode.DEFEND:
		$PlayerMode/Shield.visible = true
