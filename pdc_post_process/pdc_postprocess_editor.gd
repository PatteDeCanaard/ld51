extends EditorPlugin


func _enter_tree():
	add_autoload_singleton("PDCTools","res://addons/pdc_post_process/pdc_tools.gd")
	add_custom_type("PDCMain", "Node2D", load("res://addons/pdc_post_process/pdc_postprocess.gd"), preload("res://addons/pdc_post_process/icon.png"))
	pass


func _exit_tree():
	remove_autoload_singleton("PDCTools")
	remove_custom_type("PDCMain")
	pass
