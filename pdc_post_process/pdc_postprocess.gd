extends Node2D

class_name PDCMain
export var resolution : Vector2 = Vector2(1024,600) setget set_resolution

var viewportContainer: ViewportContainer = null
var viewport : Viewport = null
var currentSceneContainer = null
# Called when the node enters the scene tree for the first time.
func _ready():
	PDCTools.main = self
	viewportContainer = $ViewportContainer
	viewport = $ViewportContainer/Viewport
	currentSceneContainer = $ViewportContainer/Viewport/CurrentSceneContainer
	pass # Replace with function body.


func _enter_tree():
	if get_child_count()>0:
		return
	var i_vc = ViewportContainer.new()
	add_child(i_vc)
	i_vc.set_owner(get_tree().edited_scene_root)
	var i_v = Viewport.new()
	i_vc.add_child(i_v)
	i_v.set_owner(get_tree().edited_scene_root)
	set_resolution(resolution)
	var i_s = Node2D.new()
	i_v.add_child(i_s)
	i_s.set_owner(get_tree().edited_scene_root)
	i_s.name = "CurrentSceneContainer"
	pass


func _exit_tree():
	for c in get_children():
		remove_child(c)
	pass

func set_resolution(v):
	resolution = v
	if $ViewportContainer!=null:
		$ViewportContainer.rect_size = v
	if $ViewportContainer/Viewport!=null:
		$ViewportContainer/Viewport.size = v

#SCREENSHAKE
var shake_power = 6
var shake_time = 0.25
var isShake = false
var curPos = null
var elapsedtime = 0
	
func _process(delta: float) -> void:
	if isShake:
		shake(delta)

## screenshake
## power: puissance de mouvement en pixel
## duration: temps de mouvement en seconde
func screenshake(power,duration):
	if(!isShake):
		curPos = currentSceneContainer.position
	isShake = true
	elapsedtime = 0
	shake_power = power
	shake_time = duration

func shake(delta):
	if elapsedtime<shake_time:
		currentSceneContainer.position =  curPos+(Vector2(randf(), randf())*2-Vector2.ONE) * shake_power
		elapsedtime += delta
	else:
		isShake = false
		elapsedtime = 0
		currentSceneContainer.position = curPos  
